/*
 * crc32_benchmark.c

 *
 *  Created on: 12. Juli 2015
 *      Author: CReisner
 */
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include <linux/time.h>
#include <asm/uaccess.h>

#include <linux/crc32c.h>
#include <linux/crypto.h>
#include <linux/err.h>
#include <linux/scatterlist.h>

#include "crc32_bench.h"


////////////////////////////////////////////////////////////////////////////////
// SSE HW implementation
////////////////////////////////////////////////////////////////////////////////
/* CRC-32C (iSCSI) polynomial in reversed bit order. */
#define POLY 0x82f63b78

/* Multiply a matrix times a vector over the Galois field of two elements,
   GF(2).  Each element is a bit in an unsigned integer.  mat must have at
   least as many entries as the power of two for most significant one bit in
   vec. */
static inline uint32_t gf2_matrix_times(uint32_t *mat, uint32_t vec)
{
    uint32_t sum;

    sum = 0;
    while (vec) {
        if (vec & 1)
            sum ^= *mat;
        vec >>= 1;
        mat++;
    }
    return sum;
}

/* Multiply a matrix by itself over GF(2).  Both mat and square must have 32
   rows. */
static inline void gf2_matrix_square(uint32_t *square, uint32_t *mat)
{
    int n;

    for (n = 0; n < 32; n++)
        square[n] = gf2_matrix_times(mat, mat[n]);
}

/* Construct an operator to apply len zeros to a crc.  len must be a power of
   two.  If len is not a power of two, then the result is the same as for the
   largest power of two less than len.  The result for len == 0 is the same as
   for len == 1.  A version of this routine could be easily written for any
   len, but that is not needed for this application. */
static void crc32c_zeros_op(uint32_t *even, size_t len)
{
    int n;
    uint32_t row;
    uint32_t odd[32];       /* odd-power-of-two zeros operator */

    /* put operator for one zero bit in odd */
    odd[0] = POLY;              /* CRC-32C polynomial */
    row = 1;
    for (n = 1; n < 32; n++) {
        odd[n] = row;
        row <<= 1;
    }

    /* put operator for two zero bits in even */
    gf2_matrix_square(even, odd);

    /* put operator for four zero bits in odd */
    gf2_matrix_square(odd, even);

    /* first square will put the operator for one zero byte (eight zero bits),
       in even -- next square puts operator for two zero bytes in odd, and so
       on, until len has been rotated down to zero */
    do {
        gf2_matrix_square(even, odd);
        len >>= 1;
        if (len == 0)
            return;
        gf2_matrix_square(odd, even);
        len >>= 1;
    } while (len);

    /* answer ended up in odd -- copy to even */
    for (n = 0; n < 32; n++)
        even[n] = odd[n];
}

/* Take a length and build four lookup tables for applying the zeros operator
   for that length, byte-by-byte on the operand. */
static void crc32c_zeros(uint32_t zeros[][256], size_t len)
{
    uint32_t n;
    uint32_t op[32];

    crc32c_zeros_op(op, len);
    for (n = 0; n < 256; n++) {
        zeros[0][n] = gf2_matrix_times(op, n);
        zeros[1][n] = gf2_matrix_times(op, n << 8);
        zeros[2][n] = gf2_matrix_times(op, n << 16);
        zeros[3][n] = gf2_matrix_times(op, n << 24);
    }
}

/* Apply the zeros operator table to crc. */
static inline uint32_t crc32c_shift(uint32_t zeros[][256], uint32_t crc)
{
    return zeros[0][crc & 0xff] ^ zeros[1][(crc >> 8) & 0xff] ^
           zeros[2][(crc >> 16) & 0xff] ^ zeros[3][crc >> 24];
}

/* Block sizes for three-way parallel crc computation.  LONG and SHORT must
   both be powers of two.  The associated string constants must be set
   accordingly, for use in constructing the assembler instructions. */
#define LONG 8192
#define LONGx1 "8192"
#define LONGx2 "16384"
#define SHORT 256
#define SHORTx1 "256"
#define SHORTx2 "512"

/* Tables for hardware crc that shift a crc by LONG and SHORT zeros. */
static uint32_t crc32c_long[4][256];
static uint32_t crc32c_short[4][256];

struct crc32c_hw_priv {
    u32 init;
    u32 size;
    char *data;
};

/* Initialize tables for shifting crcs. */
static void *crc32c_hw_init(const char *data, u32 size)
{
	struct crc32c_hw_priv *priv = kmalloc(sizeof(struct crc32c_hw_priv), GFP_KERNEL);

    crc32c_zeros(crc32c_long, LONG);
    crc32c_zeros(crc32c_short, SHORT);


       /* no mem */
       if(priv) {
       	priv->data = (char *)data;
   		priv->size = size;
   		priv->init = 0;
       }

       return priv;
}

/* Compute CRC-32C using the Intel hardware instruction. */
static uint32_t crc32c_hw_proc(void *priv)
{
	struct crc32c_hw_priv *cp = (struct crc32c_hw_priv *) priv;

    const unsigned char *next = cp->data;
    const unsigned char *end;
    u64 crc0, crc1, crc2;      /* need to be 64 bits for crc32q */
    u32 len = cp->size;

   /* pre-process the crc */
    crc0 = cp->init ^ 0xffffffff;

    /* compute the crc for up to seven leading bytes to bring the data pointer
       to an eight-byte boundary */
    while (len && ((uintptr_t)next & 7) != 0) {
        __asm__("crc32b\t" "(%1), %0"
                : "=r"(crc0)
                : "r"(next), "0"(crc0));
        next++;
        len--;
    }

    /* compute the crc on sets of LONG*3 bytes, executing three independent crc
       instructions, each on LONG bytes -- this is optimized for the Nehalem,
       Westmere, Sandy Bridge, and Ivy Bridge architectures, which have a
       throughput of one crc per cycle, but a latency of three cycles */
    while (len >= LONG*3) {
        crc1 = 0;
        crc2 = 0;
        end = next + LONG;
        do {
            __asm__("crc32q\t" "(%3), %0\n\t"
                    "crc32q\t" LONGx1 "(%3), %1\n\t"
                    "crc32q\t" LONGx2 "(%3), %2"
                    : "=r"(crc0), "=r"(crc1), "=r"(crc2)
                    : "r"(next), "0"(crc0), "1"(crc1), "2"(crc2));
            next += 8;
        } while (next < end);
        crc0 = crc32c_shift(crc32c_long, crc0) ^ crc1;
        crc0 = crc32c_shift(crc32c_long, crc0) ^ crc2;
        next += LONG*2;
        len -= LONG*3;
    }

    /* do the same thing, but now on SHORT*3 blocks for the remaining data less
       than a LONG*3 block */
    while (len >= SHORT*3) {
        crc1 = 0;
        crc2 = 0;
        end = next + SHORT;
        do {
            __asm__("crc32q\t" "(%3), %0\n\t"
                    "crc32q\t" SHORTx1 "(%3), %1\n\t"
                    "crc32q\t" SHORTx2 "(%3), %2"
                    : "=r"(crc0), "=r"(crc1), "=r"(crc2)
                    : "r"(next), "0"(crc0), "1"(crc1), "2"(crc2));
            next += 8;
        } while (next < end);
        crc0 = crc32c_shift(crc32c_short, crc0) ^ crc1;
        crc0 = crc32c_shift(crc32c_short, crc0) ^ crc2;
        next += SHORT*2;
        len -= SHORT*3;
    }

    /* compute the crc on the remaining eight-byte units less than a SHORT*3
       block */
    end = next + (len - (len & 7));
    while (next < end) {
        __asm__("crc32q\t" "(%1), %0"
                : "=r"(crc0)
                : "r"(next), "0"(crc0));
        next += 8;
    }
    len &= 7;

    /* compute the crc for up to seven trailing bytes */
    while (len) {
        __asm__("crc32b\t" "(%1), %0"
                : "=r"(crc0)
                : "r"(next), "0"(crc0));
        next++;
        len--;
    }

    /* return a post-processed crc */
    return (uint32_t)crc0 ^ 0xffffffff;
}
void crc32c_hw_teardown(void *p)
{
	kfree(p);
}
////////////////////////////////////////////////////////////////////////////////
// Crypto API implementation
////////////////////////////////////////////////////////////////////////////////
struct crypto_priv {
    struct crypto_hash *hash;
    struct scatterlist sg;
    struct hash_desc desc;
};

void *crypto_api_setup(const char *data, u32 size)
{
    struct crypto_priv *priv = kmalloc(sizeof(struct crypto_priv), GFP_KERNEL);
    u8 init[4] = { 0,0,0,0 };
    /* no mem */
    if(priv == NULL)
        return priv;

    /* crypto api */
    priv->hash = crypto_alloc_hash("crc32c", 0, CRYPTO_ALG_ASYNC);
    if (IS_ERR(priv->hash)) {
        printk(KERN_INFO "crypto_alloc_hash failed\n");
        kfree(priv);
        return NULL;
    }


    priv->desc.tfm = priv->hash;
    priv->desc.flags = 0;

    crypto_hash_init(&priv->desc);
    crypto_hash_setkey(priv->hash, init , 4);

    sg_init_one(&priv->sg, data, size);

    return priv;
}

u32 crypto_api_proc(void *priv)
{
    struct crypto_priv *cp = (struct crypto_priv *) priv;
    u32 ret = 0;
    u8 crc[4];

    crypto_hash_update(&cp->desc, &cp->sg, cp->sg.length);
    crypto_hash_final(&cp->desc, crc);

    ret = (crc[3]<<24) + (crc[2]<<16) + (crc[1]<<8) + crc[0];
    //printk(KERN_INFO "crypto_api_proc(): data_len %d, ret 0x%08X\n", cp->sg.length, ret);

    return ret;
}

void  crypto_api_teardown(void *priv)
{
    struct crypto_priv *cp = (struct crypto_priv *) priv;

    crypto_free_hash(cp->hash);
    kfree(cp);
}
////////////////////////////////////////////////////////////////////////////////
// libcrc32 API implementation
////////////////////////////////////////////////////////////////////////////////
struct libcrc32c_priv {
    u32 init;
    u32 size;
    char *data;
};

void *libcrc32c_setup(const char *data, u32 size)
{
    struct libcrc32c_priv *priv = kmalloc(sizeof(struct libcrc32c_priv), GFP_KERNEL);

    /* no mem */
    if(priv) {
    	priv->data = (char *)data;
		priv->size = size;
		priv->init = 0;
    }

    return priv;
}

u32 libcrc32c_proc(void *priv)
{
    struct libcrc32c_priv *cp = (struct libcrc32c_priv *) priv;
    u32 ret = 0;

    if(cp)
        ret = crc32c(cp->init, cp->data, cp->size);
    //printk(KERN_INFO "libcrc32c_proc(): data_len %d, ret 0x%08X\n", cp->size, ret);

    return ret;
}

void  libcrc32c_teardown(void *priv)
{
    kfree(priv);
}
////////////////////////////////////////////////////////////////////////////////

#define MAX_CS_IMPLEMENTATIONS 	10
#define MAX_DATA_SIZE   		655360



static struct crc32_bench {
	struct crc32_impl cs_tests[MAX_CS_IMPLEMENTATIONS];
	u32 crc32_impl_cnt;
	spinlock_t lock;

	struct dentry *parent;
	struct dentry *blob;
	char data[MAX_DATA_SIZE];
	ssize_t data_size;
} inst;

static int crc32_bench_add_implementation(const char *name,
								   void *(*func_setup)(const char *d, u32 s),
								   u32 (*func_proc)(void *p),
								   void (*func_teardown)(void *p))
{
	if(inst.crc32_impl_cnt >= MAX_CS_IMPLEMENTATIONS)
		return -EIO;

	spin_lock(&inst.lock);
	inst.cs_tests[inst.crc32_impl_cnt].name = name;
	inst.cs_tests[inst.crc32_impl_cnt].func_setup 	 = func_setup;
	inst.cs_tests[inst.crc32_impl_cnt].func_proc 	 = func_proc;
	inst.cs_tests[inst.crc32_impl_cnt].func_teardown = func_teardown;
	inst.crc32_impl_cnt++;
	spin_unlock(&inst.lock);
	return 0;
}

void perform_all_checksum_tests(char *data, ssize_t n)
{
    u32 i;
    struct timespec start, end;
    void *priv = NULL;
    struct crc32_impl *t;

    for(i = 0; i < inst.crc32_impl_cnt; i++) {
    	t = &inst.cs_tests[i];
        if(t->func_setup)
            priv = t->func_setup(data, n);

        if(t->func_proc) {
			getnstimeofday(&start);
			t->digest = t->func_proc(priv);
			getnstimeofday(&end);
			if((end.tv_sec - start.tv_sec) < 0) {
				t->nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
			} else {
				t->nsec = end.tv_nsec - start.tv_nsec;
			}
        }

        if(t->func_teardown)
        	t->func_teardown(priv);

        printk(KERN_INFO  "%s: value 0x%08X, time %9lu, bytes %d\n",
        		t->name, t->digest, t->nsec, n);
    }
}

ssize_t blob_read(struct file *fp, char __user *user, size_t size, loff_t *off)
{
    copy_to_user(user, inst.data,(size < inst.data_size) ? size : inst.data_size);
    return (ssize_t) (size < inst.data_size) ? size : inst.data_size;
}
ssize_t blob_write(struct file *fp, char __user *user, size_t size, loff_t *off)
{
    copy_from_user(inst.data, user, (size < MAX_DATA_SIZE) ? size : MAX_DATA_SIZE);
    inst.data_size = (size < MAX_DATA_SIZE) ? size : MAX_DATA_SIZE;

    return inst.data_size;
}

int blob_close(struct inode *np, struct file *fp)
{
	printk(KERN_INFO "Performing benchmarks. Data length: %d bytes\n", inst.data_size);
    perform_all_checksum_tests(inst.data, inst.data_size);
    return 0;
}

static struct file_operations fops = {
        .read = blob_read,
        .write = blob_write,
        .release = blob_close,
};


static int crc32_bench_init(void)
{
    struct debugfs_blob_wrapper bw = {
            .data = inst.data,
            .size = MAX_DATA_SIZE,
    };
    printk(KERN_INFO "crc32_bench says 'Hello World'\n");

    spin_lock_init(&inst.lock);
    inst.crc32_impl_cnt = 0;
    inst.data_size = 0;
    inst.parent = NULL;
    inst.blob = NULL;

    if(!(inst.parent = debugfs_create_dir("crc32_bench", NULL)))
        return -EIO;

    if(!(inst.blob = debugfs_create_file("data", 0666, inst.parent, &bw, &fops)))
        return -EIO;

    crc32_bench_add_implementation("libcrc32c", libcrc32c_setup,
    							   libcrc32c_proc, libcrc32c_teardown);
	crc32_bench_add_implementation("cryptocrc32c", crypto_api_setup,
			                       crypto_api_proc, crypto_api_teardown);
	crc32_bench_add_implementation("hwcrc32c", crc32c_hw_init,
						crc32c_hw_proc, crc32c_hw_teardown);

    return 0;
}

static int crc32_bench_exit(void)
{
    if(inst.parent)
        debugfs_remove_recursive(inst.parent);

    return 0;
}


module_init(crc32_bench_init)
module_exit(crc32_bench_exit)

MODULE_AUTHOR("Clemens Reisner");
MODULE_DESCRIPTION("libcrc32c and intel sse4 crc performance benchmark");
MODULE_LICENSE("GPL");
