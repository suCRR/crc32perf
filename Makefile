ARCH=x86
CROSS_COMPILE=


obj-m := crc32_bench.o

all: 
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
