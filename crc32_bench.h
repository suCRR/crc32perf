/*
 * crc32_bench.h
 *
 *  Created on: 15. Juli 2015
 *      Author: clemens
 */

#ifndef CRC32_BENCH_H_
#define CRC32_BENCH_H_



struct crc32_impl {
    u32 digest;
    u64 nsec;
    char *name;
    void *(*func_setup)(const char *d, u32 s);
    u32 (*func_proc)(void *p);
    void (*func_teardown)(void *p);
};

#endif /* CRC32_BENCH_H_ */
